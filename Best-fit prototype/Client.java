import java.io.*;
import java.net.*;
import java.util.ArrayList; 
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.SAXException;
public class Client
{

    static int core = 0;
    static int serverNo = 0;
    static int jobNo = 0;
    static String serverType;
    static int count = 0;
    static String algorithm="";
    static ArrayList<String> types;                
    static ArrayList<String[]> initialCap;      //added 
    static ArrayList<String[]> servers;         // added
    static ArrayList<Integer> coreCounts;         // added
  
	//added for best fit
	static int bestFit = Integer.MAX_VALUE;
	static int minAvail = Integer.MAX_VALUE;
	static int bestFitServerID;
	static String bestFitServerType;
	static int fitnessValue;  
	static int jobCores;
	static int jobMemory;
	static int jobDisk;  
	static int altFitServerID;
	static String altFitServerType;
	static Boolean bestFitFound = false;  
	static Boolean findingAlt = false;
	static Boolean altFitFound = false;

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException{
		
		Socket client = new Socket("127.0.0.1", 50000);
	
		OutputStream outputstream = client.getOutputStream();
        DataOutputStream output = new DataOutputStream(outputstream);
	
		PrintWriter pw = new PrintWriter(outputstream, true);
		pw.print("HELO\n");
		pw.flush();	
		String username = System.getProperty("user.name");
		pw.print("AUTH " + username + "\n" );
		pw.flush();	

        serverResponse(client, pw);
		pw.close();
        output.close();

        client.close();
    
    }

    public static void serverResponse(Socket client, PrintWriter pw) throws ParserConfigurationException, IOException, SAXException{
	BufferedReader input = new BufferedReader(new InputStreamReader(client.getInputStream()));
        String message;
        while((message = input.readLine())!=null){

       		clientResponse(pw, message);

       }
	input.close();
    }
   

    public static void clientResponse(PrintWriter pw, String message)  throws ParserConfigurationException, IOException, SAXException{
        if(message.equals("OK")){
	    	if(count > 0){
	        types = ReadXML("system.xml");
		
     	        pw.print("REDY\n");
	        pw.flush();
	    }
	    count++;
	}

	else if(message.equals(".")){

		System.out.println(jobNo);
		
		if(bestFitFound == true){
			 
			pw.print("SCHD " + jobNo + " " + bestFitServerType + " " + bestFitServerID + "\n");
			pw.flush();

			//reset values
			minAvail = Integer.MAX_VALUE;
			bestFit = Integer.MAX_VALUE;
			bestFitFound = false;

		} else if(altFitFound == true){

			pw.print("SCHD " + jobNo + " " + altFitServerType + " " + altFitServerID + "\n");
			pw.flush();
			minAvail = Integer.MAX_VALUE;
			bestFit = Integer.MAX_VALUE;
			altFitFound = false;
			findingAlt = false;
			
			
		} else	{

			pw.print("RESC Capable "+ jobCores + " " + jobMemory + " " + jobDisk + "\n");
	        pw.flush();
			findingAlt = true;

		}
		
	}
	
	else { //REDY -> JOBN
              
	    String msg = message.substring(0,4);
        String[] splitmsg = message.split("\\s+");
	    if(msg.equals("JOBN")){
	        
			jobNo = Integer.parseInt(splitmsg[2]);

			//Updating job info 

			jobCores = Integer.parseInt(splitmsg[4]);
			jobMemory = Integer.parseInt(splitmsg[5]);
			jobDisk = Integer.parseInt(splitmsg[6]);

            pw.print("RESC Avail "+ splitmsg[4] + " " + splitmsg[5] + " " + splitmsg[6] + "\n");
	       
	        pw.flush();
            }
	    else if(msg.equals("DATA")){
	        pw.print("OK\n");
	        pw.flush();
	    }
            else if(msg.equals("NONE")){
            pw.print("QUIT\n");
	        pw.flush();
            }
	    else if(msg.equals("ERR:")){
            pw.print("OK\n");
	        pw.flush();
	    }
 	    //If the array contains 7 items, the message contains the server information.
	    else if(splitmsg.length==7){ 
                 

			//Server characteristics
			String currServerType = splitmsg[0];
			int currServerID = Integer.parseInt(splitmsg[1]);
			int currServerState = Integer.parseInt(splitmsg[2]);
			int currAvailTime = Integer.parseInt(splitmsg[3]);
			int currServerCores = Integer.parseInt(splitmsg[4]);
			int currServerMemory = Integer.parseInt(splitmsg[5]);
			int currServerDisk = Integer.parseInt(splitmsg[6]);

			
			      
			//Best-fit - Checks if curent server is sufficient for job
			
			//When recieving DATA, if RESEC Capable was used, it will look for available servers based on initial resource capacity
			if(findingAlt == true){
				
				fitnessValue = currServerCores - jobCores;
				
				if((fitnessValue < bestFit)|| (fitnessValue == bestFit && currAvailTime < minAvail)){
					
					bestFit = fitnessValue;
					minAvail = currAvailTime;
					altFitServerID = currServerID;
					altFitServerType = currServerType;
					altFitFound = true;

				}


			} 
			//might not need this since RESC Avail should only return servers with sufficient resource

			else if((jobCores <= currServerCores) && (jobMemory <= currServerMemory) && (jobDisk <= currServerDisk)){
					
				fitnessValue = currServerCores - jobCores;

					//if fitness value is lower than bestfit and server time is lower than minimum time, update current server info as new bestfit and minimum time
					if(currServerState != 4){
				
						if((fitnessValue < bestFit) || (fitnessValue == bestFit && currAvailTime < minAvail)){

						bestFit = fitnessValue;
						minAvail = currAvailTime;
						bestFitServerID = currServerID;
						bestFitServerType = currServerType;
						bestFitFound = true;

					}

				}
					
			}
			
			pw.print("OK\n");
			pw.flush();
		 
		
	    }
        }
	
    }
    
    
    public static ArrayList<String> ReadXML(String xml)  throws ParserConfigurationException, IOException, SAXException{
        ArrayList<String> types = new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(xml));
        
	NodeList nodeList = document.getDocumentElement().getChildNodes();
	//Prints root element config
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
 	    if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;
                NodeList nList1 = elem.getChildNodes();
                for (int j = 0; j < nList1.getLength(); j++) {
		    Node node1 = nList1.item(j);
 	   	    if (node1.getNodeType() == Node.ELEMENT_NODE) {
			Element elem2 = (Element)node1;
			NamedNodeMap attributes = elem2.getAttributes();
			for (int k = attributes.getLength()-1; k > -1; k--) {
			    Attr attr = (Attr) attributes.item(k);
			    String attrName = attr.getNodeName();
 			    String attrValue = attr.getNodeValue();
			    if(attrName.equals("type")){
			    	types.add(attrValue);	
			    }	
    		       }
		    }  
	        }
            }
        }
	
    return types;

    }
}

